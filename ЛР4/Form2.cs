﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ЛР4
{
    public partial class Form2 : Form
    {
        public Form2(string name)
        {
            InitializeComponent();
            label2.Text = "Вітаю в TravelTeam, " + name;
        }

        string place;
        string theDate;
        int countPeople;
        int countNight;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            place = comboBox1.Text.ToString();
            label6.Visible = true;
            dateTimePicker1.Visible = true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            theDate = dateTimePicker1.Value.ToShortDateString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            countPeople = int.Parse(textBox1.Text);
            countNight = int.Parse(textBox2.Text);
            label7.Text = "Ваш тур у " + place + " на дату " + theDate + " на" + countNight + " ночей з кількістю " + countPeople + " людей був заброньовано";

        }
    }
}
